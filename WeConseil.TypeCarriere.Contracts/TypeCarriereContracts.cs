﻿namespace WeConseil.TypeCarriere.Contracts
{
    public record TypeCarriereItemCreated(Guid TypeCarriereId, string Designation);
    public record TypeCarrieretemUpdated(Guid TypeCarriereId, string Designation);
    public record TypeCarriereItemDeleted(Guid TypeCarriereId);
}
