using Microsoft.AspNetCore.Mvc;
using Macro.Common;
using WeConseil.TypeCarriere.Service.Dtos;
using WeConseil.TypeCarriere.Service.Entities;


namespace WeConseil.TypeCarriere.Service.Controllers
{
    //https://localhost:7185/swagger/index.html
    [ApiController]
    [Route("TypeCarriere")]
    public class TypeCarriereController : ControllerBase
    {
        private readonly IRepository<TypeCarriereItem> tcarriereRepository;
        public TypeCarriereController(IRepository<TypeCarriereItem> tcarriereRepository)
        {
            this.tcarriereRepository = tcarriereRepository;
        }
        //GET /typeCriters
        [HttpGet]
        public async Task<ActionResult<IEnumerable<TypeCarriereItemDtos>>> GetAsync()
        {
            var items = (await tcarriereRepository.GetAllAsync()).Select(item => item.AsDto());
            return Ok(items);
        }
        //GET /typeCriters/123456
        [HttpGet("{id}")]
        public async Task<ActionResult<TypeCarriereItemDtos>> GetByIdAsync(Guid id)
        {
            var item = await tcarriereRepository.GetAsync(id);
            if (item == null)
            {
                return NotFound();
            }
            return item.AsDto();
        }
        //POST /typeCriters
        [HttpPost]
        public async Task<ActionResult<TypeCarriereItemDtos>> PostAsync(CreateTypeCarriereItemDtos createTypeCarriereItemDtos)
        {
            var itemTypeCarriere = new TypeCarriereItem
            {
                Designation = createTypeCarriereItemDtos.Designation,
                CreateDate = DateTimeOffset.UtcNow,
                IsDeleted = false

            };
            await tcarriereRepository.CreateAsync(itemTypeCarriere);
            return CreatedAtAction(nameof(GetByIdAsync), new { id = itemTypeCarriere.Id }, itemTypeCarriere);
        }
        //Update /typeCriters
        [HttpPut("{id}")]
        public async Task<IActionResult> PutAsync(Guid id, UpdateTypeCarriereItemDtos updateTypeCarriereItemDtos)
        {
            var existingTypeCarriereItem = await tcarriereRepository.GetAsync(id);
            if (existingTypeCarriereItem == null)
            {
                return NotFound();
            }
            existingTypeCarriereItem.Designation = updateTypeCarriereItemDtos.Designation;
            await tcarriereRepository.UpdateAsync(existingTypeCarriereItem);
            return NoContent();
        }
        //Update /typeCriters
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteAsync(Guid id)
        {
            var existingTypeCarriereItem = await tcarriereRepository.GetAsync(id);
            if (existingTypeCarriereItem == null)
            {
                return NotFound();
            }
            existingTypeCarriereItem.IsDeleted = true;
            await tcarriereRepository.UpdateAsync(existingTypeCarriereItem);
            return NoContent();
        }
    }
}