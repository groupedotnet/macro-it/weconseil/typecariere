using WeConseil.TypeCarriere.Service.Dtos;
using WeConseil.TypeCarriere.Service.Entities;

namespace WeConseil.TypeCarriere.Service
{
    public static class Extensions
    {
        public static TypeCarriereItemDtos AsDto (this TypeCarriereItem typeCarriereItem)
        {
             return new TypeCarriereItemDtos(typeCarriereItem.Id,typeCarriereItem.Designation);
        }
    }
}