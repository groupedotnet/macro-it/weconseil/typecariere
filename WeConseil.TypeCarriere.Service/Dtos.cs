using System.ComponentModel.DataAnnotations;

namespace WeConseil.TypeCarriere.Service.Dtos
{
    public record TypeCarriereItemDtos(Guid Id, string Designation);
    public record CreateTypeCarriereItemDtos([Required][MinLength(3)][MaxLength(60)] string Designation);
    public record UpdateTypeCarriereItemDtos([Required][MinLength(3)][MaxLength(60)] string Designation);
}