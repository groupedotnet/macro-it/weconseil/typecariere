using Macro.Common;

namespace WeConseil.TypeCarriere.Service.Entities
{

    public class TypeCarriereItem : IEntity
    {
        public Guid Id { get; set; }
        public string Designation { get; set; }
        public DateTimeOffset CreateDate { get; set; }
        public Boolean IsDeleted { get; set; }
    }
}